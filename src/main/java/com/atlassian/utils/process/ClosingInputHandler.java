package com.atlassian.utils.process;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A simple {@link InputHandler} that immediately closes the process's {@code stdin} stream.
 */
public class ClosingInputHandler extends BaseInputHandler {

    private static final Logger log = Logger.getLogger(ClosingInputHandler.class);

    public void process(OutputStream input) {
        try {
            input.close();
        } catch (IOException e) {
            log.error("Exception caught while closing the stream", e);
        }
    }
}
