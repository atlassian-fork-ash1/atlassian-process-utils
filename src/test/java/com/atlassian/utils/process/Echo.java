package com.atlassian.utils.process;

import java.nio.charset.Charset;

public class Echo {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
        for (String arg : args) {
            try {
                // using the error stream to write debug information
                System.err.println("Echo: received param     " + CallEcho.getBytes(arg) + " in encoding " + Charset.defaultCharset());
            } catch (Exception e) {
                // no-op
            }
        }

		String sep = "";
		for (String str : args) {
			System.out.println(sep + str);
			sep = " ";
		}
	}

}
