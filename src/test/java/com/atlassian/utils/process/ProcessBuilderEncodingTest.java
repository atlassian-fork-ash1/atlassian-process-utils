package com.atlassian.utils.process;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ProcessBuilderEncodingTest {

    private static class EchoResult {
        public String input;
        public String output;
    }

    /*
     * Starts the CallEcho as an separate process. This is necessary because the jvm uses the default encoding
     * (-Dfile.encoding) for encoding command line arguments that are provided to external processes.This test tests
     * whether encodings that should be compatible are actually compatible. This is needed for
     * http://jira.atlassian.com/browse/CRUC-4793 which provides a workaround for a JVM bug on windows.
     */
    protected EchoResult spawnEcho(String encoding, String... testStrings) throws Exception {
        List<String> cmd = new ArrayList<String>(Arrays.asList(
                "java", "-Dfile.encoding=" + encoding, "-Dsun.jnu.encoding=" + encoding, CallEcho.class.getName()));
        cmd.addAll(Arrays.asList(testStrings));

        ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        processBuilder.environment().put("CLASSPATH", System.getProperty("java.class.path"));
        processBuilder.environment().put("LC_ALL", "en_US." + encoding.toUpperCase());
        processBuilder.environment().put("LANG", "en_US." + encoding.toUpperCase());
        final Process process = processBuilder.start();

        Thread th = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                         if (process.getErrorStream().read() == -1) {
                             break;
                         }
                    } catch (IOException e) {
                        //
                    }
                }
            }
        }) ;
        th.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        try {
            EchoResult result = new EchoResult();
            result.input = reader.readLine();
            result.output = reader.readLine();

            return result;
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    protected void assertNotEquals(String message, String value1, String value2) {
        assertFalse(message + "; result was : " + value1, (value1 == null && value2 == null) || (value1 != null && value1.equals(value2)));
    }

    protected boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    @Test
    public void testIncompatibleCommandLineArgumentsASCII() throws Exception {
        EchoResult result = spawnEcho("ASCII", "iso-8859-1");
        assertNotEquals("ASCII - incompatibility expected", result.input, result.output);

        result = spawnEcho("ASCII", "unicode-chinese");
        assertNotEquals("ASCII - incompatibility expected", result.input, result.output);
    }

    /**
     * This test will only succeed on windows if the ANSI code page that is defined is compatible with iso-8859-1 (e.g. cp1252)
     *
     * @throws Exception -
     */
    @Test
    public void testCommandLineArgumentsUTF8() throws Exception {
        EchoResult result = spawnEcho("UTF-8", "iso-8859-1");
        assertEquals("UTF-8 - incompatibility detected:", result.input, result.output);
    }

    /**
     * This test is expected to fail on windows (assuming the ANSI CP is cp1252) because on windows that code page
     * is used to encode the commandline arguments.
     *
     * @throws Exception -
     */
    @Test
    public void testCommandLineArgumentsUTF8IncompatibleWithCp1252() throws Exception {
        EchoResult result = spawnEcho("UTF-8", "unicode-chinese");

        if (isWindows()) {
            assertNotEquals("UTF-8 - incompatibility expected", result.input, result.output);
        } else {
            assertEquals("UTF-8 - incompatibility detected", result.input, result.output);
        }
    }

    @Test
    public void testCommandLineArgumentsWin1252() throws Exception {
        if (Charset.isSupported("windows-1252") && isWindows()) {
            // test is disabled in linux because (ubuntu) linux always uses UTF-8 to encode parameters, causing this test to fail
            EchoResult result = spawnEcho("windows-1252", "iso-8859-1");
            assertEquals("Windows cp 1252 -  incompatibility detected:", result.input, result.output);
        }
    }

    @Test
    public void testCommandLineArgumentsWin1252Iso88591Incompatibilities() throws Exception {
        if (Charset.isSupported("windows-1252") && isWindows()) {
            // other platforms claim to support windows-1252, but it looks like only iso-8859-1 is supported.
            EchoResult result = spawnEcho("windows-1252", "win-1252-not-in-iso-8859-1");
            assertEquals("Windows cp 1252 -  incompatibility detected:", result.input, result.output);
        }
    }

    @Test
    public void testCommandLineArgumentsWin437() throws Exception {
        if (Charset.isSupported("windows-437")) {
            EchoResult result = spawnEcho("windows-437", "windows-437");
            assertEquals("Windows cp 437 - incompatibility detected:", result.input, result.output);
        }
    }

    @Test
    public void testCommandLineArgumentsIso88591() throws Exception {
        if (isWindows()) {
            // test is disabled in linux because (ubuntu) linux always uses UTF-8 to encode parameters, causing this test to fail
            EchoResult result = spawnEcho("iso-8859-1", "iso-8859-1");
            assertEquals("ISO-8859-1 - incompatibility detected:", result.input, result.output);
        }
    }
}
